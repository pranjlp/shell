#Overview

This project tries to create a unix shell with a subset of well known unix programs inbuilt, like ls, cp, rm. It also allows "exec"-ing binaries in the current directory, forking new process in background and allows pipes and redirect operators. Undertaken to explore nix stdlib libraries and C code.
